## Starting a web project with Webpack and Babel

When starting a new web project, you often use some libraries. Nowadays, these libraries usually come with tools to scaffold your project. However, in certain cases you don’t want to rely on these libraries. For that reason, we’ll demonstrate how you can set up your own project with Webpack and Babel.

#### Getting started
Before starting, you have to install a recent version of Node.js and npm. Once that’s done, you can initialize your project using the npm init command. This command will help you setting up a package.json, which could look like this:
```
{
  "name": "webpack-babel-test",
  "version": "0.0.1",
  "author": {
    "name": "dritanxhezo"
  },
  "private": true,
  "scripts": {
  },
}
```

#### Setting up Babel
The next step is to set up Babel. Babel is a transpiler, which means it translates code from language A to language B. For Babel, this is a translation between JavaScript and JavaScript.

You might think, why should I do that? Well, each browser and JavaScript platform has a different support for certain features. Using Babel, we can transpile the JavaScript code so that it works on certain browsers. This allows developers to use the most recent language features, while keeping support for all targeted platforms.

To set up Babel, I’m going to add a few development dependencies, such as:

* @**babel/core:** This dependency is the core library, and is required when using Babel.
* @**babel/cli:** As the name suggests, this dependency allows us to use a command prompt/terminal/npm scripts to transpile resources using Babel.
* @**babel/register:** This library will do the heavy lifting for you, and will automatically transpile all resources that use Node.js require().
* @**babel/preset-env:** Next to all these “core” dependencies, you also have to define which environment you want to compile to. Using this dependency, you can easily target certain environments.

```npm install --save-dev @babel/core @babel/cli @babel/preset-env @babel/register```

After installing these dependencies, the next step is to configure Babel to use the @babel/preset-env preset. To do so, we create a file called .babelrc:
```
{
  "presets": [
    ["@babel/preset-env", {
      "targets": "> 0.25%, not dead"
    }]
  ]
}
```
This configuration makes sure that the transpiled code works on all browsers that are:

* Still supported, and thus not dead or abandoned.
* Have a market share of more than 0,25%.

You can also target specific browsers, such as:
```
{
  "presets": [
    ["@babel/preset-env", {
      "targets": {
        "chrome": "80"
      }
    }]
  ]
}
```
Rather than configuring these targets within .babelrc, you can also choose to configure them within package.json or within a .browserlistrc file. Personally, I prefer having all Babel-related configuration at one place.

#### Setting up Webpack

While Babel does the transpiling for you, there are still some other things you need to do. For example, you have to build everything, set up linting, … . Tools like Webpack can make this a lot easier.

To be able to use Webpack, we need to add some other dependencies first, such as:

* **webpack:** This is the core Webpack dependency
* **webpack-cli:** This dependency allows you to run Webpack from your commandline, terminal, or use it within npm scripts.
* **webpack-dev-server:** This dependency will set up a local web server for development purposes. This means that you don’t have to install and configure your own web server like Apache or nginx. Additionally, this will automatically refresh the page when you change your code.

Additionally, we have to install a few loaders and plugins, that will be used for bundling the application:

* **babel-loader:** This will allow us to transpile the code using Babel, and then bundle it
* **css-loader:** The CSS loader will be able to load CSS files, and imported CSS files. This allows you to add your styling to your bundle.
* **style-loader:** This loader is usually used in combination with the css-loader, and will bundle the CSS by adding a script that injects a <style> tag.
* **html-webpack-plugin:** This plugin will allow you to automatically add a <script> tag containing your bundle to your index page.

```
npm install --save-dev webpack webpack-cli webpack-dev-server babel-loader css-loader style-loader html-webpack-plugin 
```

#### Configuring Webpack

The next step is to configure how Webpack should use these loaders and plugins. The way to do this is by creating a file called webpack.config.js.

First of all, I had to tell Webpack which my entry points are, and where the bundle should be located. These entry points are basically a list of your main files that import all the other ones.

```
const path = require('path');
const webpack = require('webpack');
const htmlWebpackPlugin = require('html-webpack-plugin');
require('@babel/register');

module.exports = env => {
  return {
    entry: ['./src/index.js', './src/style.css'],
    output: {
      path: path.resolve(__dirname, 'dist/'),
      filename: 'bundle.js'
    },
    devtool: 'source-map'
    // ...
  };
};
```

In this case, both src/index.js and src/style.css are my entrypoints. Thus, all files that are imported from there on, will also be bundled. The location of the bundle is a file called bundle.js, which we’ll store in a folder called /dist. Next to my bundle, I’ll also generate a source map file that I can use for development.

The next step is to tell Webpack which loaders to use to bundle your source code. As mentioned earlier, I will be using the babel-loader, css-loader and style-loader:

```
const path = require('path');
const webpack = require('webpack');
const htmlWebpackPlugin = require('html-webpack-plugin');
require('@babel/register');

module.exports = env => {
  return {
    // entry + output + devtool ...
    module: {
      rules: [{
        test: /\.js$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }, {
        test: /\.css$/,
        exclude: /node_modules/,
        use: ['style-loader', 'css-loader']
      }]
    },
    // ...
  };
};
```

Additionally, I also want to add my src/index.html page to the destination, and add a <script> tag to it containing the bundle.js, and a hash that will make sure browsers are not using an old cached version:

```
const path = require('path');
const webpack = require('webpack');
const htmlWebpackPlugin = require('html-webpack-plugin');
require('@babel/register');

module.exports = env => {
  return {
    // entry + output + devtool + module ...
    plugins: [
      new htmlWebpackPlugin({
        template: 'src/index.html',
        filename: 'index.html',
        hash: true
      })
    ]
  };
};
```

Finally, I also want to define an environment variable called API_URL that will contain a reference to my backend, so that I can customise this when building for a different environment:
```
const path = require('path');
const webpack = require('webpack');
const htmlWebpackPlugin = require('html-webpack-plugin');
require('@babel/register');

module.exports = env => {
  return {
    // entry + output + devtool + module ...
    plugins: [
      new htmlWebpackPlugin({
        template: 'src/index.html',
        filename: 'index.html',
        hash: true
      }),
      new webpack.DefinePlugin({
        'API_URL': JSON.stringify(env.API_URL)
      })
    ]
  };
};
```
With that, we’re able to set up our initial project and bundle our HTML, CSS and JS files together into the dist/ folder.

#### Running webpack
Even though we completely configured webpack for now, we still would have to execute the webpack command ourselves, and pass the API_URL environment variable.

To make our life easier, we can use npm scripts to actually do this for us. This also allows us to call the Webpack CLI without having to install it globally. Installing it globally could be troublesome in case you want to use multiple versions.

To add a script, you can open the package.json file and add something like this:
```
{
  "name": "webpack-babel-test",
  "version": "0.0.1",
  "scripts": {
    "start": "webpack-dev-server --open --mode development --port 8081 --env.API_URL=http://localhost:8080/api",
    "build": "webpack --mode production --env.API_URL=./webpack-babel-test/api"
  },
  "devDependencies": {
    "@babel/cli": "^7.2.3",
    "@babel/core": "^7.2.2",
    "@babel/preset-env": "^7.3.1",
    "@babel/register": "^7.0.0",
    "babel-loader": "^8.0.5",
    "css-loader": "^2.1.0",
    "html-webpack-plugin": "^3.2.0",
    "style-loader": "^0.23.1",
    "webpack": "^4.29.0",
    "webpack-cli": "^3.2.1",
    "webpack-dev-server": "^3.1.14"
  }
}
```
These scripts will do two things. First of all, when running npm start or npm run start, it will use the webpack-dev-server dependency to serve your application on port 8081. Additionally, it will configure the API_URL environment variable.

The other script, which can be launched with npm run build, will generate production-ready code, and will configure the API_URL to a different value. In this case, I’ll use a reverse proxy and configure the backend on the same port, so I can use a relative path.

With this, we’re properly able to run our application with Webpack and Babel.


### Containerizing your static web project

We’ve seen how we can set up a simple web project using Babel and Webpack. More precisely, we’ve seen how we can both run and build the application.
In this article, we’ll see how we can containerize such a project using Docker.

#### Setting up a Dockerfile
The first step is to create a Dockerfile containing the steps necessary to run the application within a Docker container. To run some HTML, CSS and JavaScript code, we have to serve it somewhere. That means that we’ll have to deploy it on a webserver like nginx.

So, if we create a Dockerfile, it will likely start like this:
```
FROM nginx:1.15.8-alpine
```
The next step is to properly copy our bundle to the right location so nginx will properly serve it. For nginx that location is /usr/share/nginx/html, so we’ll use the COPY command like this:
```
FROM nginx:1.15.8-alpine
COPY dist/ /usr/share/nginx/html
```
#### Using npm scripts
Now that we have our Dockerfile, the next step is to build it. To build it, we could use the following Docker command:

```docker build -t dritanxhezo/webpack-babel-test:0.0.1 . ```

However, a more interesting approach is to integrate this within npm. The reasoning behind this is that we can then use the same tool for building and containerizing our application.

To do this, we can add a script like this:
```
{
  "name": "webpack-babel-test",
  "version": "0.0.1",
  "author": {
    "name": "dritanxhezo"
  },
  "scripts": {
    "start": "webpack-dev-server --open --mode development --port 8081 --env.API_URL=http://localhost:8080/api",
    "build": "webpack --mode production --env.API_URL=./webpack-babel-test/api",
    "docker": "docker build -t $npm_package_author_name/$npm_package_name:$npm_package_version ."
  }
}
```
The nice thing within npm scripts is that we can use variables like $npm_package_name to refer to another property within the package.json file. This allows us to build the Docker image without duplicating any information.

##### Running your application
To run your application, you can now use the following command:

```docker run -p 80:80 dritanxhezo/webpack-babel-test:0.0.1```

This will run a Docker container and expose port 80 to the host machine as port 80. This means you can open the application on http://localhost. This might be different though if you’re running on a separate Docker machine. In that case, you’ll have to replace localhost by the IP address of your Docker machine.

#### Using Docker compose
Another nice feature of Docker is Docker Compose. With Docker Compose, we can define our configuration to run the Docker container as a YAML file. For example, since we exposed port 80 before, we could write a simple docker-compose.yml file like this in stead:

```
version: '3.7'

services:
  webpack-babel-test:
    image: dritanxhezo/webpack-babel-test:1.0.0
    ports:
      - 80:80
```

This will do the same thing as before. However, in stead of using docker run, you’ll now have to use docker-compose up.


#### Making network calls
One issue is that we’ll probably make some API calls, perhaps even to other Docker containers. As we’ve seen before, we can only communicate to Docker containers if we explicitly expose a port.

Another possibility is to have inter-container network communication. This is possible by defining dependencies between our Docker containers, and works without having to expose a port to the host machine.

However, since we’ll make these API calls within our JavaScript code, running within a web browser on a separate machine, we can only do this if we proxy these calls. Luckily, since we’re using nginx, we already have a builtin reverse proxy.

First of all, let’s add a dependency within the docker-compose.yml file:
```
version: '3.7'

services:
  webpack-babel-test:
    image: dritanxhezo/webpack-babel-test:0.0.1
    ports:
      - 80:80
    depends_on:
      - webpack-babel-test-service
```

In this example, we’re adding a dependency from this container to another container called webpack-babel-test-service. Within our Docker container, we can now communicate to that service by using the container name as its hostname, for example http://webpack-babel-test-service:8080/api/webpack-babel-test-service/@random would allow us to call our backend API.

The next step is to create an nginx.conf file that does the proxying, for example:

```
events {
    worker_connections  1024;
}

http {
    index   index.html;
    server {
        location /webpack-babel-test-service {
            proxy_pass http://webpack-babel-test-service:8080;
            proxy_http_version 1.1;
            rewrite ^/webpack-babel-test-service(.*)$ $1 break;
        }
    }
}
```
This configuration will proxy all calls going to /webpack-babel-test-service/... by routing them to http://webpack-babel-test-service:8080/....

The final step is to add a volume for this configuration file so that we can make sure that it’s in the right spot when we run our container:
```
version: '3.7'

services:
  webpack-babel-test:
    image: dritanxhezo/webpack-babel-test:0.0.1
    ports:
      - 80:80
    depends_on:
      - webpack-babel-test-service
    volumes:
      - ./nginx.conf:/etc/nginx/nginx.conf
```

This will map our local nginx.conf to /etc/nginx/nginx.conf within the container, which is the proper location so that nginx can pick it up.

The reason I didn’t include this configuration file within my image is because the proxy rule depends on another container. If I included it within my image, and I would change the name of my other container, I would have to rebuild my image.

With that, we’re now able to fully run our applications on Docker containers.